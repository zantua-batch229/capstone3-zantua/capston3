import React, { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import {Card, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Login from  './ProductAdd.js'
import { withRouter } from "react-router-dom";
import Table from 'react-bootstrap/Table';

//import axios from 'axios';

const ProductsTable = (props) => {
  const [products, setProducts] = useState([]);
  const token = localStorage.getItem('token');
  const {user} = useContext(UserContext);
  const navigate = useNavigate();





  useEffect(() => {
      fetch(`${ process.env.REACT_APP_API_URL }/users`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => { setProducts(data) });
  }, []);




  const handleSetAdmin = (productId) => {
    if(user.id !== null){
      fetch(`${ process.env.REACT_APP_API_URL }/users/${productId}/setadmin`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {



        if(data == true){
          Swal.fire({
            title: "User is now Admin",
            icon: 'success',
          });




        } else{
          Swal.fire({
            title: `${data}`,
            icon: 'error',
            text: "Pls. try again."
          })

        }

      });
    }

  
  };
    const handleUnsetAdmin = (productId) => {
    if(user.id !== null){
      fetch(`${ process.env.REACT_APP_API_URL }/users/${productId}/unsetadmin`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {



        if(data == true){
          Swal.fire({
            title: "User is set to not admin",
            icon: 'success',
          });




        } else{
          Swal.fire({
            title: `${data}`,
            icon: 'error',
            text: "Pls. try again."
          })

        }

      });
    }

  
  };



  return (
    (user.isUserAdmin !== true) 
      ?
        <Navigate to="/"/>
      :
    <>


      <Table striped responsive="sm">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Mobile Number</th>
            <th>Is Admin</th>
          </tr>
        </thead>
        <tbody>
          {products.map(product => (
            <tr key={product._id}>
              <td>{product.firstName}</td>
              <td>{product.lastName}</td>
              <td>{product.email}</td>
              <td>{product.mobile}</td>
              <td>{product.isAdmin.toString()}</td>
              
              <td>
                <Button onClick={() => handleSetAdmin(product._id)}>Set Admin</Button>
                <Button onClick={() => handleUnsetAdmin(product._id)}>Unset Admin</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>



    </>
  );
};

export default ProductsTable;

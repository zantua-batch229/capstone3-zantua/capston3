import React, { useState, useEffect, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {Card, Button} from 'react-bootstrap';
import Typography from '@material-ui/core/Typography';
import ProductCard from '../components/ProductCard.js'
import Modal from '@material-ui/core/Modal';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Login from  './Login.js'
import Swal from 'sweetalert2';

const useStyles = makeStyles({
  card: {
    minWidth: 275,
    margin: 10,
  },
});




function ProductsList() {
  const [products, setProducts] = useState([]);
  const classes = useStyles();

  const [editingProduct, setEditingProduct] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  
  const {user} = useContext(UserContext);
  const navigate = useNavigate();


  useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/products`)
          .then(res => res.json())
          .then(data => { setProducts(data) });

  }, []);


  const token = localStorage.getItem('token');
  const handleAdd = (productId) => {
    
    if(user.id !== null){
      fetch(`${ process.env.REACT_APP_API_URL }/carts/add`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({
          productId: productId,
          quantity: 1
        })
      })
      .then(res => res.json())
      .then(data => {

        console.log(productId);
        console.log(user.id);
        console.log(localStorage.getItem('token'));
        console.log(data);

        if(data == true){
          Swal.fire({
            title: "Item added!",
            icon: 'Success',
          });


          navigate("/login");


        } else{
          Swal.fire({
            title: "add failed!",
            icon: 'error',
            text: "Pls. try again."
          })

        }

      });


    }else{
      navigate("/login");
    }
    // Add code to delete the specified product from the database
  };

  function viewDetails(productID){
    localStorage.setItem('itemid', productID);
    localStorage.setItem('mode', 'view');
    navigate("/productEdit");
  }
const cardStyle = {
      transition: 'all 0.2s ease-in-out',
   };

   const cardHoverStyle = {
      borderColor: 'blue',
   };

  return (

    <>
    <Grid container spacing={3}>
      {products.map(product => (
        <Grid item xs={12} sm={6} md={4} key={product._id}>
          <Card
          style={cardStyle}
               onMouseOver={e => {
                  e.currentTarget.style.borderColor = 'blue';
               }}
               onMouseOut={e => {
                  e.currentTarget.style.borderColor = 'gray';
               }} 

          className="mt-3" style={{ width: '18rem' }}>
          <Card.Img onClick={() => viewDetails(product._id)} variant="top" src={product.image} />
            <Card.Body>
              <Card.Title>{product.name}</Card.Title>
              <Card.Text>
              {(product.isOnSale)?
              <span>
                <td style={{color: "red"}}><s>P {product.price}</s></td>
                <td style={{paddingLeft: "10px"}}>P {product.salePrice}</td>
                </span>
              :
                <td>P {product.price}</td>
              }

              </Card.Text>
                

                <Button size="lg" variant="outline-success" onClick={() => handleAdd(product._id)}>Add to Cart</Button>
            </Card.Body>
          </Card>
          {/*<ProductCard key={product._id} productProps={product}/>*/}
        </Grid>
      ))}
    </Grid>


      </>    
  );
}

export default ProductsList;

import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function AddEditProduct() {

	// State Hooks -> store values of the input fields
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [salePrice, setSalePrice] = useState('');
	const [image, setImage] = useState('');
	const [stock, setStock] = useState('');
	const [isActive, setIsActive]= useState(false);

	const token = localStorage.getItem('token');


	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();


	useEffect(() => {

		if(name !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);

		}
	})


	function add(e){
		// prevents page redirection via form submission
		e.preventDefault();
		console.log(e);


		fetch(`${ process.env.REACT_APP_API_URL }/products/add`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
          			"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					salePrice: salePrice,
					image: image,
					stock: stock,
				})
			})
			.then(res => res.json())
			.then(data => {

				//console.log(data);

				if(data == true){
					Swal.fire({
						title: "Successfully added!",
						icon: 'Success',
						text: "You have successfully added a product."
					});


					navigate("/products");


				} else{
					Swal.fire({
						title: "Add product failed!",
						icon: 'error',
						text: "Pls. try again."
					})

				}

			});

		

		
	}

	return (
		(user.isAdmin !== true) 
			?
				<Navigate to="/"/>
			:

		<Form onSubmit={(e) =>add(e)}>
	      <Form.Group className="mb-3" id="name">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control 
	        placeholder="Name" 
	        value={name} 
	        onChange={e => setName(e.target.value)} 
	        required />
	      </Form.Group>
	      <Form.Group className="mb-3" id="description">
	        <Form.Label>Description</Form.Label>
	        <Form.Control 
	        placeholder="Description" 
	        value={description} 
	        onChange={e => setDescription(e.target.value)} 
	        required />
	      </Form.Group>
	      <Form.Group className="mb-3" id="price">
	        <Form.Label>Price</Form.Label>
	        <Form.Control 
	        placeholder="price" 
	        value={price} 
	        onChange={e => setPrice(e.target.value)} 
	        required />
	      </Form.Group>
	      <Form.Group className="mb-3" id="salePrice">
	        <Form.Label>Sale Price</Form.Label>
	        <Form.Control 
	        placeholder="salePrice" 
	        value={salePrice} 
	        onChange={e => setSalePrice(e.target.value)} 
	        required />
	      </Form.Group>
	      
	      <Form.Group className="mb-3" id="image">
	        <Form.Label>image</Form.Label>
	        <Form.Control 
	        placeholder="image" 
	        value={image} 
	        onChange={e => setImage(e.target.value)} 
	        required />
	      </Form.Group>


      
	      	{(isActive) ?
		      	<Button variant="primary" type="submit" id="submitBtn">
		        	Save
		      	</Button> 
		      	:
		      	<Button variant="primary" type="submit" id="submitBtn" disabled>
		        	Save
		      	</Button> 
		    }
	      
	      

	      
    	</Form>
	)
}
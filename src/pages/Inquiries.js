import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Message() {

	// State Hooks -> store values of the input fields
	const [email, setEmail] = useState('');
	const [fName, setfName] = useState('');
	
	const [mobile, setMobile] = useState('');
	const [lName, setlName] = useState('');

	const [isActive, setIsActive]= useState(false);


	const {user, setUser} = useContext(UserContext);

	const navigate = useNavigate();


	useEffect(() => {

		if((email !== '' && lName !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);

		}
	})


	function sendMessage(e){
		// prevents page redirection via form submission
		e.preventDefault();
		console.log(e);


		fetch(`${ process.env.REACT_APP_API_URL }/messages/send`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email,
					name: fName,
					merssage: lName,
					mobile: mobile,
				})
			})
			.then(res => res.json())
			.then(data => {

				//console.log(data);

				if(data == true){
					Swal.fire({
						title: "Inquiry Sent!",
						icon: 'Success',
						text: "We will contact you as soon as possible."
					});


					navigate("/");


				} else{
					Swal.fire({
						title: "Inquiry failed!",
						icon: 'error',
						text: "Pls. try again."
					})

				}

			});

		

		
	}

	return (

		<Form onSubmit={(e) => sendMessage(e)}>
	      <Form.Group className="mb-3" id="userFName">
	        <Form.Label>Name</Form.Label>
	        <Form.Control 
	        placeholder="Name" 
	        value={fName} 
	        onChange={e => setfName(e.target.value)} 
	        required />
	      </Form.Group>
	      
	      <Form.Group className="mb-3" id="userMobile">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control 
	        placeholder="Mobile Number" 
	        value={mobile} 
	        onChange={e => setMobile(e.target.value)} 
	        required />
	      </Form.Group>
	      
	      <Form.Group className="mb-3" id="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        type="email" 
	        placeholder="Enter email" 
	        value={email} 
	        onChange={e => setEmail(e.target.value)} 
	        required />
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>
	      <Form.Group className="mb-3" id="userLName">
	        <Form.Label>Message</Form.Label>
	        <Form.Control 
	        placeholder="Message" 
	        value={lName} 
	        onChange={e => setlName(e.target.value)} 
	        required />
	      </Form.Group>



	      
	      
		      	<Button variant="primary" type="submit" id="submitBtn">
		        	Send Message
		      	</Button> 
	      
	      
    	</Form>
	)
}
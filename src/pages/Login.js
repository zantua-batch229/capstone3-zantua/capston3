import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';


export default function Login(props){

	const {user, setUser} = useContext(UserContext);

	const [userName, setUserName] = useState('');
	const [password, setPassword] = useState('');
	const [isInputMet, setIsInputMet] = useState(false);

	

	useEffect(() => {

		if(userName !== '' && password !== ''){
			setIsInputMet(true);
		} else {
			setIsInputMet(false);
		}


	})

	const retrieveUserDetails = (accessToken) =>{
		// The token will be sent as part of the request's header information
		// We put "Bearer" in front of the token to follow implementation standards for JWTs

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);
			
			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin,
				isUserAdmin: data.isUserAdmin,
				email: data.email,
				firstName: data.firstName
			})
			props.history.goBack();
		})

	}
	
	return(
			
			(user.id !== null) 
			?
				<Navigate to="/"/>
			:
				<Form onSubmit={(e) => Login(e)}>
					<h1>LOGIN</h1>
					<Form.Group className="mb-3" id="userName">
						<Form.Label>User Name</Form.Label>
						<Form.Control 
							type="email" 
							placeholder="Email" 
							required 
							value={userName} 
							onChange={e => setUserName(e.target.value)}/>
					</Form.Group>

					<Form.Group className="mb-3" id="password">
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password" 
							placeholder="Password" 
							required 
							value={password} 
							onChange={e => setPassword(e.target.value)}/>
					</Form.Group>

					
					{
						isInputMet 
						?
						<Button variant="success" type="submit" id="loginBtn">
			        	Login
			      		</Button>
			      		:
			      		<Button variant="success" type="submit" id="loginBtn" disabled>
			        	Login
			      		</Button>

		      		}
		      		

				</Form>




		)


	function Login(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: userName,
				password: password
			})
		})
		.then(resultFromRequest => resultFromRequest.json())
		.then(data => {
			//console.log(data)

			// If no user information is found, the "access" property will not be available and will return undefined
			// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				//console.log(data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to EZ Shop!"
				})

				localStorage.setItem('email', userName);

				/*setUser({
					email: localStorage.getItem('email')
					
				})*/

				setUserName('');
				setPassword('');
				setIsInputMet(false);
				
			} else{
				Swal.fire({
					title: "Login failed.",
					icon: "error",
					text: "Check your login details and try again."	
				})
			}

		})


	}

	

}
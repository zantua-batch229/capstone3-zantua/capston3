import React, { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import {Card, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { withRouter } from "react-router-dom";
import Table from 'react-bootstrap/Table';

//import axios from 'axios';

const Inquiries = () => {
  const [messages, setMessages] = useState([]);
  const token = localStorage.getItem('token');
  const {user} = useContext(UserContext);
  const navigate = useNavigate();





  useEffect(() => {
      fetch(`${ process.env.REACT_APP_API_URL }/messages/`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => { 
        setMessages(data) });
  }, []);


  return (
    (user.isAdmin !== true) 
      ?
        <Navigate to="/"/>
      :
    <>


      <Table striped responsive="sm">
        <thead>
          <tr>
            <th>Date</th>
            <th>Name</th>
            <th>Message</th>
            <th>Email</th>
            <th>Mobile Number</th>

          </tr>
        </thead>
        <tbody>
          {messages.map(message => (
            <tr key={message._id}>
             <td>{message.date}</td>
              <td>{message.name}</td>
              <td>{message.message}</td>
              <td>{message.email}</td>
              <td>{message.mobile}</td>
              
            </tr>
          ))}
        </tbody>
      </Table>



    </>
  );
};

export default Inquiries;


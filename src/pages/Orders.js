import React, { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import {Card, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Login from  './ProductAdd.js'
import { withRouter } from "react-router-dom";
import Table from 'react-bootstrap/Table';



function AllOrders() {
  const [orders, setOrders] = useState([]);
  const {user, setUser} = useContext(UserContext);
  const token = localStorage.getItem('token');  
  const navigate = useNavigate();

  useEffect(() => {
    fetch(`${ process.env.REACT_APP_API_URL }/orders`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => { setOrders(data) 

    });

  }, []);

  if (!orders || orders.length === 0) {
    return (
      <>
      <h3>ALL ORDERS</h3>
      <div>No Orders Found.</div>
      </>

      );
  }
  const handleUpdate = (product) => {
    localStorage.setItem('itemid', product);
    navigate("/productEdit");
  };

  function getItemName(productID){
  const token = localStorage.getItem('token');
  fetch(`${ process.env.REACT_APP_API_URL }/products/${productID}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
          }
        })
        .then(res => res.json())
        .then(data => {
          return data.name;
        })
  }

  return (
    <>
    <h3>ALL ORDERS</h3>
    <Table>
      <thead>
        <tr>
          <th style={{width: "10%", color: "#6C3483"}}>Action</th>
          <th style={{width: "10%", color: "#6C3483"}}>Status</th>
          <th style={{width: "20%", color: "#6C3483"}}>Purchased On</th>
          <th style={{width: "10%", color: "#6C3483"}}>Total</th>
        </tr>
      </thead>
      <tbody>
        {orders.map(order => (
            <tr key={order._id}>
              <Button size="sm" variant="outline-success" onClick={() => handleUpdate(order._id)}>Update Status</Button>
              <td>{order.status}</td>
              <td>{order.purchasedOn}</td>
              <td>{order.totalAmount}</td>
              <td>
                <Table>
                  <thead>
                    <tr>
                      <th style={{width: "20%", color: "#070C71"}}>Item Name</th>
                      <th style={{width: "20%", color: "#070C71"}}>Quantity</th>
                      <th style={{width: "20%", color: "#070C71"}}>Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    {order.products.map((item) => (
                      <tr key={item._id}>
                        <td>{getItemName(item.productId)}</td>
                        <td>{item.quantity}</td>
                        <td>{item.unitPrice}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </td>              
            </tr>

          ))}

      </tbody>
    </Table>
    </>
  );
}




export default AllOrders;


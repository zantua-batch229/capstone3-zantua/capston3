import Banner from  '../components/Banner.js'
import Catalogue from  './Products2.js'
import {Card, Button} from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import cardImage1 from '../assets/card-image-4.jpg';
import cardImage2 from '../assets/card-image-2.jpg';
import cardImage3 from '../assets/card-image-3.jpg';
import { useNavigate, Navigate } from 'react-router-dom';



const data = {
	title: "EZ Shop",
	content: "your one stop shop computer needs",
	destination: "/products2",
	label: "View Calaogue"
}


export	default function Home(){
	const navigate = useNavigate();
	 const handleShop = () => {
    
	    navigate("/catalogue");
	  };
	  const handleInquire = () => {
    
	    navigate("/inquiries");
	  };
  
	return(
		<>
	      {/*<Banner data={data}/>*/}

	    <Container className="my-4">
	         <Row>
	            <Col md={4}>
	               <Card>
	                  <Card.Img style={{height: "200px"}} variant="top" src={cardImage1} />
	                  <Card.Body>
	                     <Card.Title>Computers and Devices</Card.Title>
	                     <Card.Text>
	                        "Shop for the latest computer technology at our online store! Choose from a wide selection of laptops, desktops, and accessories from top brands. Enjoy fast and reliable shipping, easy returns, and expert customer support. Upgrade your computer experience today!"
	                     </Card.Text>
	                     <Button variant="primary" onClick={() => handleShop()}>Shop Now</Button>
	                  </Card.Body>
	               </Card>
	            </Col>
	            <Col md={4}>
	               <Card>
	                  <Card.Img style={{height: "200px"}} variant="top" src={cardImage2} />
	                  <Card.Body>
	                     <Card.Title>Software Development</Card.Title>
	                     <Card.Text>
	                        "Transform your ideas into reality with our software development services. Our team of experts utilizes the latest technology to design and develop custom software solutions that meet your unique needs. We deliver high-quality results on time and within budget.
	                     </Card.Text>
	                     <Button variant="primary" onClick={() => handleInquire()}>Inquire Now</Button>
	                  </Card.Body>
	               </Card>
	            </Col>
	            <Col md={4}>
	               <Card>
	                  <Card.Img style={{height: "200px"}} variant="top" src={cardImage3} />
	                  <Card.Body>
	                     <Card.Title>Computer Assembly and Repairs</Card.Title>
	                     <Card.Text>
	                        "Get fast and reliable computer repairs from the experts. Our experienced technicians can handle any issue, big or small, for all brands and models. Enjoy same-day service, affordable pricing, and a warranty on all repairs. Trust us to get your computer back up and running smoothly."
	                     </Card.Text>
	                     <Button variant="primary"onClick={() => handleInquire()}>Message Us</Button>
	                  </Card.Body>
	               </Card>
	            </Col>
	         </Row>
	      </Container>
	    </>
	);
}



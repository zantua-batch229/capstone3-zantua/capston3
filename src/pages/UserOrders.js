import React, { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import {Card, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Login from  './ProductAdd.js'
import { withRouter } from "react-router-dom";
import Table from 'react-bootstrap/Table';



function UserOrders() {
  const [orders, setOrders] = useState([]);
  const {user, setUser} = useContext(UserContext);
  const token = localStorage.getItem('token');  

  useEffect(() => {
    fetch(`${ process.env.REACT_APP_API_URL }/orders/${user.id}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => { setOrders(data) });

  }, []);

  if (!orders || orders.length === 0) {
    return (
      <>
      
      <h3>User Orders</h3>
      <div>No Orders Found.</div>
      </>

      );
  }

  function getItemName(productID){
  const token = localStorage.getItem('token');
  fetch(`${ process.env.REACT_APP_API_URL }/products/${productID}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
          }
        })
        .then(res => res.json())
        .then(data => {
          return data.name;
        })
  }

  return (
    <>
    <h3>{user.email} Orders History</h3>
    <Table>
      <thead>
        <tr>
          <th style={{width: "20%", color: "#6C3483"}}>Purchased On</th>
          <th style={{width: "10%", color: "#6C3483"}}>Status</th>
          <th style={{width: "10%", color: "#6C3483"}}>Total</th>
        </tr>
      </thead>
      <tbody>
        {orders.map(order => (
            <tr key={order._id}>
              <td>{order.purchasedOn}</td>
              <td>{order.status}</td>
              <td>{order.totalAmount}</td>
              <td>
                <Table>
                  <thead>
                    <tr>
                      <th style={{width: "20%", color: "#070C71"}}>Item Name</th>
                      <th style={{width: "20%", color: "#070C71"}}>Quantity</th>
                      <th style={{width: "20%", color: "#070C71"}}>Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    {order.products.map((item) => (
                      <tr key={item._id}>
                        <td>{getItemName(item.productId)}</td>
                        <td>{item.quantity}</td>
                        <td>{item.unitPrice}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </td>              
            </tr>

          ))}

      </tbody>
    </Table>
    </>
  );
}




export default UserOrders;


/*function Orders() {
  const [orders, setOrders] = useState([]);
  const {user, setUser} = useContext(UserContext);
  const token = localStorage.getItem('token');

  useEffect(() => {
    const fetchOrders = async () => {
      const response = await fetch(`${ process.env.REACT_APP_API_URL }/orders/${user.id}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      });
      setOrders(response.data);
    };

    fetchOrders();
  }, []);

  return (
    <table>
      <thead>
        <tr>
          <th>Order ID</th>
          <th>Customer Name</th>
          <th>Order Items</th>
        </tr>
      </thead>
      <tbody>
        {orders.map((order) => {
          const fetchCustomer = async () => {
            const res = await fetch(`${ process.env.REACT_APP_API_URL }/users/details/${user.id}`, {
                method: "GET",
                headers: {
                  "Content-Type": "application/json",
                  "Authorization": `Bearer ${token}`
                }
              });
            return res.data.firstName + ' ' + res.data.lastName;
          };

          return (
            <tr key={order._id}>
              <td>{order._id}</td>
              <td>{fetchCustomer()}</td>
              <td>
                <table>
                  <thead>
                    <tr>
                      <th>Item Name</th>
                      <th>Quantity</th>
                      <th>Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    {order.items.map((item) => (
                      <tr key={item._id}>
                        
                        <td>{item.quantity}</td>
                        <td>{item.unitprice}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default Orders;

*/



/*



const Cart = () => {
  const [cartItems, setCartItems] = useState([]);
  const [productData, setProductData] = useState([]);
  const token = localStorage.getItem('token');
  const {user, setUser} = useContext(UserContext);
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalItems, setTotalItems] = useState(0);


  useEffect(() => {
    (async () => {
      const response = await fetch(`${ process.env.REACT_APP_API_URL }/orders/${user.id}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      });
      const data = await response.json();
      setCartItems(data);

      const productPromises = data.map(async (item) => {
        const productResponse = await fetch(
          `${ process.env.REACT_APP_API_URL }/products/${item.productId}`, {
              method: "GET",
              headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
              }
            }
        );
        return await productResponse.json();
      });

      setProductData(await Promise.all(productPromises));

      let amount = 0;
      let itemCount = 0;
      productData.forEach((product, index) => {
        amount += product.price * cartItems[index].quantity;
        itemCount += cartItems[index].quantity;

      });
      setTotalAmount(amount);
      setTotalItems(itemCount);

    })();
  }, []);
  const navigate = useNavigate();
  const handleCheckout = () => {
    
  fetch(`${ process.env.REACT_APP_API_URL }/carts/checkout`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        },
      })
      .then(res => res.json())
      .then(data => {

        //console.log(data);

        if(data == true){
          Swal.fire({
            title: "Successfully Checked out!",
            icon: 'Success',
          });


          navigate("/products");


        } else{
          Swal.fire({
            title: "Checkout failed!",
            icon: 'error',
            text: "Pls. try again."
          })

        }

      });

          
  };

  return (
    <>
    <Table>
      <thead>
        <tr>
          <th>Product</th>
          <th>Quantity</th>
          <th>Price</th>
          <th style={{ textAlign: "right" }}>Sub-Total</th>
        </tr>
      </thead>
      <tbody>
        {productData.map((product, index) => (
          <>
          
          <tr key={cartItems[index].id}>
            <td>{product.name}</td>
            <td>{cartItems[index].quantity}</td>
            <td>{product.price}</td>
            <td style={{ textAlign: "right" }}>{product.price * cartItems[index].quantity}</td>
          </tr>
          </>
        ))}
      </tbody>
    </Table>
    <div style={{ display: "flex", justifyContent: "space-between" }}>
      <h3>Total Items: {totalItems}</h3>
      <h3>Total Amount: {totalAmount}</h3>
    <Button variant="primary" onClick={() => handleCheckout()}>
        Checkout
    </Button>  
    </div>
    


    
    </>
  );
};

export default Cart;


*/
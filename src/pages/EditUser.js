import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';



export default function EditUser(props) {

	// State Hooks -> store values of the input fields

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [fName, setfName] = useState('');
	const [lName, setlName] = useState('');
	const [mobile, setMobile] = useState('');

	const token = localStorage.getItem('token');

	const navigate = useNavigate();
	const [isActive, setIsActive]= useState(false);


	useEffect(() => {

		if((email !== '' && password1 !== '' && password2 !== '' && (password1 === password2) && fName !== '' && lName !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);

		}

		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
	        method: "GET",
	        headers: {
	          "Content-Type": "application/json",
	          "Authorization": `Bearer ${token}`
	        }
	      })
	      .then(res => res.json())
	      .then(data => { 
	      	console.log(data);
	      	setfName(data.firstName);
	      	setlName(data.lastName);
	      	setMobile(data.mobile);
	      	setEmail(data.email);
	      	});
	},[])


	function UpdateUser(e){
		// prevents page redirection via form submission
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/users/update`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					email: email,
					password: password1,
					firstName: fName,
					lastName: lName,
					mobileNo: mobile,
				})
			})
			.then(res => res.json())
			.then(data => {

				//console.log(data);

				if(data == true){
					Swal.fire({
						title: "Successfully Updated!",
						icon: 'success',
						text: "You have successfully registered."
					});


					navigate("/login");


				} else{
					Swal.fire({
						title: "Registration failed!",
						icon: 'error',
						text: "Pls. try again."
					})

				}

			});
			props.history.goBack();

		

		
	}

	return (

		<Form onSubmit={(e) => UpdateUser(e)}>
	      <Form.Group className="mb-3" id="userFName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control 
	        placeholder="First Name" 
	        value={fName} 
	        onChange={e => setfName(e.target.value)} 
	        required />
	      </Form.Group>
	      <Form.Group className="mb-3" id="userLName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control 
	        placeholder="Last Name" 
	        value={lName} 
	        onChange={e => setlName(e.target.value)} 
	        required />
	      </Form.Group>
	      <Form.Group className="mb-3" id="userMobile">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control 
	        placeholder="Mobile Number" 
	        value={mobile} 
	        onChange={e => setMobile(e.target.value)} 
	        required />
	      </Form.Group>
	      
	      <Form.Group className="mb-3" id="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        type="email" 
	        placeholder="Enter email" 
	        value={email} 
	        onChange={e => setEmail(e.target.value)} 
	        required />
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" id="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        type="password" 
	        placeholder="Password" 
	        value={password1} 
	        onChange={e => setPassword1(e.target.value)} 
	        required />
	      </Form.Group>

	      <Form.Group className="mb-3" id="password2">
	        <Form.Label>Re-Enter Password</Form.Label>
	        <Form.Control 
	        type="password" 
	        placeholder="Re-Enter Password" 
	        value={password2} 
	        onChange={e => setPassword2(e.target.value)} 
	        required />
	      </Form.Group>


	      
	      
	      	
		      	<Button variant="primary" type="submit" id="submitBtn">
		        	Update
		      	</Button> 
	      	
	      

	      
    	</Form>
	)
}
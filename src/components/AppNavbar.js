 import {useContext} from 'react';

import { Navbar, Nav, Image } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import { useNavigate, Navigate } from 'react-router-dom';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext.js';
import logo from '../assets/logo.png';


export default function AppNavbar(){
	//const [user, setUser] = useState(localStorage.getItem('email'));
	const navigate = useNavigate();
	const {user} = useContext(UserContext);
	let userType = '';
	if(user.isUserAdmin){
		userType = 'Owner';
	} else {
		if(user.isAdmin){
			userType = 'Admin';
		}else{
			userType = 'User';
		}
	}


	return(
		<Navbar style={{backgroundColor: "#37BAD4"}} variant="dark" expand="lg" className="p-3">
		<Container>
			<Navbar.Brand as={NavLink} exact to="/">EZ
			<Image src={logo} width="40" height="40" className="d-inline-block align-top" />
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
	
				<Nav className="ml-auto">



					<Nav.Link as={NavLink} exact to="/">Home</Nav.Link>
					
	
					{
						(user.isAdmin) ?
							(user.isUserAdmin)?
							<>
							<Nav.Link as={NavLink} exact to="/products">Products</Nav.Link>
							<Nav.Link as={NavLink} exact to="/users">Users</Nav.Link>
							<Nav.Link as={NavLink} exact to="/allOrders">All Orders</Nav.Link>
							<Nav.Link as={NavLink} exact to="/users">Reports</Nav.Link>
							<Nav.Link as={NavLink} exact to="/allInquiries">Inquiries</Nav.Link>
							<Nav.Link as={NavLink} exact to="/logout">Logout</Nav.Link>
							</>

							:
							<>
							<Nav.Link as={NavLink} exact to="/products">Products</Nav.Link>
							<Nav.Link as={NavLink} exact to="/allOrders">All Orders</Nav.Link>
							{/*<Nav.Link as={NavLink} exact to="/users">Reports</Nav.Link>*/}
							<Nav.Link as={NavLink} exact to="/allInquiries">Inquiries</Nav.Link>
							<Nav.Link as={NavLink} exact to="/logout">Logout</Nav.Link>
							</>

							

						:
							(user.email !== null) ?	
							<>
								<Nav.Link as={NavLink} exact to="/catalogue">Catalogue</Nav.Link>
								<Nav.Link as={NavLink} exact to="/carts">Cart</Nav.Link>
								<Nav.Link as={NavLink} exact to="/userOrders">Orders</Nav.Link>
								<Nav.Link as={NavLink} exact to="/logout">Logout</Nav.Link>

							</>
							:
							<>
								<Nav.Link as={NavLink} exact to="/catalogue">Catalogue</Nav.Link>
{/*								<Nav.Link as={NavLink} exact to="/carts">Cart</Nav.Link>
								<Nav.Link as={NavLink} exact to="/userOrders">Orders</Nav.Link>*/}
								<Nav.Link as={NavLink} exact to="/register">Register</Nav.Link>
								<Nav.Link as={NavLink} exact to="/login">Login</Nav.Link>
								
							</>

					}
					
				</Nav>
				{ (user.email !== null) ?
					<>
						<Navbar.Collapse className="justify-content-end">
			          		<Navbar.Text>
			            		Signed in as: <a href="/editUser">{user.email + ' (' + userType +  ')'}</a>
			          			</Navbar.Text>
			        	</Navbar.Collapse>
		        	</>
		        	: <></>
	        	}
			</Navbar.Collapse>
		</Container>
		</Navbar>

		)
}
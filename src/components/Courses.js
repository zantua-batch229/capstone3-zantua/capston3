import ProductCard from '../components/ProductCard.js'
import { useEffect, useState, useContext } from 'react'
import UserContext from '../UserContext.js';
import { useNavigate, Navigate } from 'react-router-dom';

export default function Courses(){

	const [products, setProducts] = useState([]);
	  const token = localStorage.getItem('token');
	  const {user} = useContext(UserContext);
	  const navigate = useNavigate();

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/allproducts`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {
	            

	            setProducts(data.map(product => {
	            	console.log(product);
	            	return(

	            		<ProductCard key={product._id} productProps={product}/>

	            		)
	            }))

	      });
	}, [])
	
	return (
		<>
			<h1>Products</h1>
			{products}
		</>
		)

	
}
